#!/usr/bin/env python
# encoding: utf-8

import requests as rq

#url = 'https://docs.google.com/forms/d/e/1FAIpQLSeO90IBVT4XJcSsvZCkSjVKJrgEZcFKl3OU2clo7c3WeSGkFw/formResponse'
url = 'https://docs.google.com/forms/d/e/1FAIpQLSdD16SiTeb0XLEAB8VsSGxtID-zhlL6tyc6TbgtrI17Nh7FSg/formResponse'

payload = {
    'entry.1324830674' : '33.5',
    'entry.199153875' : '123066811',
    'fvv' : '1',
    'draftResponse' : '[]',
    'pageHistory' : '0',
    'fbzx' : '8750856068936102963'
}

try:
    payload['entry.1324830674'] = "37.5" # To be input from thermo detector
    payload['entry.199153875'] = "108122" # To be input from barcode reader.
    res = rq.post(url, data=payload)
    res.raise_for_status()
    if res.status_code == 200 :
        print('Fill Out : ' + payload['entry.199153875'] )
except rq.HTTPError:
    print('HTTP Error!')
