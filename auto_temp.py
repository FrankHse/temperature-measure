#!/usr/bin/env python
# How to setup the GrovePi. https://github.com/DexterInd/GrovePi
# 2x16 LCD plate. https://learn.adafruit.com/adafruit-16x2-character-lcd-plus-keypad-for-raspberry-pi
# sudo pip3 install adafruit-circuitpython-charlcd
# encoding: utf-8

# For accessing PiCamera
from picamera import PiCamera
# For accessing GrovePi
from grovepi import *
from grove_rgb_lcd import *

from PIL import Image
import subprocess
from datetime import datetime
import requests as rq

# 
def meter_on():
    # Trigger the therm-meter 
    Relay_pin = 2
    pinMode(Relay_pin, "OUTPUT")
    print(".")
    digitalWrite(Relay_pin,1)
    print(".")
    time.sleep(0.5)
    digitalWrite(Relay_pin,0)
    print("meter is on!")
 
# Take a picutre from picamera with temperature and student id.
def picamera_take_a_picture(pic_file_path):
    pic_file_fd = open(pic_file_path, "wb")
    camera = PiCamera()
    # rotation the picture
    camera.rotation = -90
    camera.capture(pic_file_fd)
    pic_file_fd.close()
    camera.close()


# extract the student id
def extract_id(barcode_file):
    # calling zbarimg to get the student identification from picture.
    cmd=["/usr/bin/zbarimg", "-q", barcode_file]
    try:
        student_id = subprocess.check_output(cmd, stderr=None)
        student_id = student_id.decode("utf-8")
        if ":" in student_id:
            student_id_t = student_id[student_id.find(":")+1:-1]
        print("Student ID is " + student_id_t)
        return student_id_t
    except subprocess.CalledProcessError as e:
        return "XXXX"

# extract the temperature
def extract_temp(temp_file):
    # calling sccor to get the readable body temperature from picture.
    cmd=["/usr/bin/ssocr", "-T", "-d", "-1", temp_file]
    #cmd=["/usr/bin/ssocr", "invert", "-T", "-d", "7", temp_file]
    try:
        temperature = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        # truncate the last two ".0"
        temperature = temperature.decode("utf-8")[:4]
        if temperature[2] != '.':
            temp1 = temperature[0]+temperature[1]+'.'+temperature[2]
            temperature = temp1
        print("Body temperature is " + temperature)
        return temperature
    except subprocess.CalledProcessError as e:
        #raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
        return "0.0"

# Display the student id and temperature on Grove-LCD.
def display(sid, temp, color):
    setText("")
    if color == "green":
        setRGB(0, 255, 0)
    elif color == "red":
        setRGB(255, 0, 0)
    else:
        setRGB(0, 0, 255)
    setText_norefresh("ID:" + sid + "\nTEMP:" + temp)


# Main routine.
def do_measure():
    now =  datetime.today()
    file_prefix = "/tmp/" + now.strftime("%Y%m%d_%H%M%S")
    #print(file_prefix)
    pic_file_path = file_prefix + ".png"
    print(pic_file_path)
    picamera_take_a_picture(pic_file_path)

    # Open the image
    img = Image.open(pic_file_path).convert("L")
    
    # rotateion the image a little bit
    # img = img.rotate( -1.5, expand=1 )
    
    # Cut the temperature area of the image
    x, y, w, h = 1265, 740, 380, 320 # Adjust if you move the meter.
    temp_img = img.crop((x, y, x+w, y+h))
    for i in range(temp_img.size[0]):
        for j in range(temp_img.size[1]):
            if(temp_img.getpixel((i,j)) > 130):
                temp_img.putpixel((i,j),255)
            else:
                temp_img.putpixel((i,j), 0)
    temp_img.save(file_prefix + "tmp.png")
    temp_file_path =  file_prefix + "tmp.png"
    temp = extract_temp(temp_file_path)

    # Cut the student id area of the image
    x, y, w, h =510, 830, 330, 90 # Adjust if you move the student id card.
    sid_img = img.crop((x, y, x+w, y+h))
    sid_img.save(file_prefix + "sid.png")
    barcode_file_path =  file_prefix + "sid.png"
    student_id = extract_id(barcode_file_path)
    return student_id, temp

def upload_to_cloud(sid, temp):
    url = 'https://docs.google.com/forms/d/e/1FAIpQLSdD16SiTeb0XLEAB8VsSGxtID-zhlL6tyc6TbgtrI17Nh7FSg/formResponse'
    
    payload = {
        'entry.1324830674' : '33.5',
        'entry.199153875' : '123066811',
        'fvv' : '1',
        'draftResponse' : '[]',
        'pageHistory' : '0',
        'fbzx' : '8750856068936102963'
    }
    
    try:
        payload['entry.1324830674'] = str(temp) # To be input from thermo detector
        payload['entry.199153875'] = sid # To be input from barcode reader.
        res = rq.post(url, data=payload)
        res.raise_for_status()
        if res.status_code == 200 :
            print('Fill Out : ' + payload['entry.199153875'] )
    except rq.HTTPError:
        print('HTTP Error!')


def autorun():
    ultrasonic_ranger = 4
    pinMode(ultrasonic_ranger, "INPUT")
    distant = 100
    #setRGB(0, 255, 0)
    #setText("Ready")
    try:
        while distant > 10:
            # Read distance value from Ultrasonic
            distant = ultrasonicRead(ultrasonic_ranger)
            print(distant,'cm')
            time.sleep(0.2)
            
        if distant <= 10:
            setRGB(0, 255, 0)
            setText("Measuring")
            meter_on()
            myid, mytemp = do_measure()
            if float(mytemp) >= 37.5 : 
                display(myid, mytemp, "red")
            else:
                display(myid, mytemp, "green")
            upload_to_cloud(myid, mytemp)

            
    except TypeError:
        print("Error")
    except IOError:
        print("Error")
            
if __name__ == "__main__":
    ultrasonic_ranger = 4
    pinMode(ultrasonic_ranger, "INPUT")
    distant = 100
    setRGB(0, 255, 0)
    setText("Ready")    
    try:
        while distant > 10:
            # Read distance value from Ultrasonic
            distant = ultrasonicRead(ultrasonic_ranger)
            print(distant,'cm')
            time.sleep(0.2)
            
        if distant <= 10:
            setRGB(0, 255, 0)
            setText("Measuring")            
            meter_on()
            myid, mytemp = do_measure()
            if float(mytemp) >= 37.5 : 
                display(myid, mytemp, "red")
            else:
                display(myid, mytemp, "green")
            upload_to_cloud(myid, mytemp)

            
    except TypeError:
        print("Error")
    except IOError:
        print("Error")
        

